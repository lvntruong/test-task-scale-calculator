export function nFormatter(num, digits) {
  var si = [
    { value: 1, symbol: "" },
    { value: 1e3, symbol: "k" },
    { value: 1e6, symbol: "M" },
    { value: 1e9, symbol: "G" },
    { value: 1e12, symbol: "T" },
    { value: 1e15, symbol: "P" },
    { value: 1e18, symbol: "E" },
  ];
  var rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
  var i;
  for (i = si.length - 1; i > 0; i--) {
    if (num >= si[i].value) {
      break;
    }
  }
  return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol;
}

export const advances = [
  {
    months: 2,
    contributions: [
      {
        amount: 0.15,
        fee: 0.24,
      },
      {
        amount: 0.2,
        fee: 0.17,
      },
      {
        amount: 0.25,
        fee: 0.145,
      },
      {
        amount: 0.3,
        fee: 0.1175,
      },
      {
        amount: 0.35,
        fee: 0.1025,
      },
      {
        amount: 0.4,
        fee: 0.09,
      },
      {
        amount: 0.45,
        fee: 0.0775,
      },
      {
        amount: 0.5,
        fee: 0.0755,
      },
      {
        amount: 0.55,
        fee: 0.0735,
      },
      {
        amount: 0.6,
        fee: 0.0715,
      },
      {
        amount: 0.65,
        fee: 0.0695,
      },
      {
        amount: 0.7,
        fee: 0.0675,
      },
      {
        amount: 0.75,
        fee: 0.065,
      },
      {
        amount: 0.8,
        fee: 0.063,
      },
      {
        amount: 0.85,
        fee: 0.061,
      },
      {
        amount: 0.9,
        fee: 0.059,
      },
      {
        amount: 0.95,
        fee: 0.055,
      },
      {
        amount: 1.0,
        fee: 0.0525,
      },
    ],
  },
  {
    months: 3,
    contributions: [
      {
        amount: 0.2,
        fee: 0.25,
      },
      {
        amount: 0.25,
        fee: 0.215,
      },
      {
        amount: 0.3,
        fee: 0.17,
      },
      {
        amount: 0.35,
        fee: 0.145,
      },
      {
        amount: 0.4,
        fee: 0.13,
      },
      {
        amount: 0.45,
        fee: 0.1175,
      },
      {
        amount: 0.5,
        fee: 0.1025,
      },
      {
        amount: 0.55,
        fee: 0.09,
      },
      {
        amount: 0.6,
        fee: 0.0825,
      },
      {
        amount: 0.65,
        fee: 0.0775,
      },
      {
        amount: 0.7,
        fee: 0.0755,
      },
      {
        amount: 0.75,
        fee: 0.0735,
      },
      {
        amount: 0.8,
        fee: 0.0715,
      },
      {
        amount: 0.85,
        fee: 0.07,
      },
      {
        amount: 0.9,
        fee: 0.0695,
      },
      {
        amount: 0.95,
        fee: 0.067,
      },
      {
        amount: 1.0,
        fee: 0.065,
      },
    ],
  },
  {
    months: 4,
    contributions: [
      {
        amount: 0.3,
        fee: 0.24,
      },
      {
        amount: 0.35,
        fee: 0.2,
      },
      {
        amount: 0.4,
        fee: 0.17,
      },
      {
        amount: 0.45,
        fee: 0.1575,
      },
      {
        amount: 0.5,
        fee: 0.145,
      },
      {
        amount: 0.55,
        fee: 0.13,
      },
      {
        amount: 0.6,
        fee: 0.1175,
      },
      {
        amount: 0.65,
        fee: 0.1025,
      },
      {
        amount: 0.7,
        fee: 0.0975,
      },
      {
        amount: 0.75,
        fee: 0.0925,
      },
      {
        amount: 0.8,
        fee: 0.0875,
      },
      {
        amount: 0.85,
        fee: 0.085,
      },
      {
        amount: 0.9,
        fee: 0.0825,
      },
      {
        amount: 0.95,
        fee: 0.08,
      },
      {
        amount: 1.0,
        fee: 0.0775,
      },
    ],
  },
  {
    months: 5,
    contributions: [
      {
        amount: 0.35,
        fee: 0.255,
      },
      {
        amount: 0.4,
        fee: 0.2275,
      },
      {
        amount: 0.45,
        fee: 0.2,
      },
      {
        amount: 0.5,
        fee: 0.17,
      },
      {
        amount: 0.55,
        fee: 0.1575,
      },
      {
        amount: 0.6,
        fee: 0.145,
      },
      {
        amount: 0.65,
        fee: 0.13,
      },
      {
        amount: 0.7,
        fee: 0.1175,
      },
      {
        amount: 0.75,
        fee: 0.11,
      },
      {
        amount: 0.8,
        fee: 0.1025,
      },
      {
        amount: 0.85,
        fee: 0.0995,
      },
      {
        amount: 0.9,
        fee: 0.095,
      },
      {
        amount: 0.95,
        fee: 0.09,
      },
      {
        amount: 1.0,
        fee: 0.085,
      },
    ],
  },
  {
    months: 6,
    contributions: [
      {
        amount: 0.45,
        fee: 0.24,
      },
      {
        amount: 0.5,
        fee: 0.2125,
      },
      {
        amount: 0.55,
        fee: 0.185,
      },
      {
        amount: 0.6,
        fee: 0.17,
      },
      {
        amount: 0.65,
        fee: 0.1575,
      },
      {
        amount: 0.7,
        fee: 0.1475,
      },
      {
        amount: 0.75,
        fee: 0.14,
      },
      {
        amount: 0.8,
        fee: 0.13,
      },
      {
        amount: 0.85,
        fee: 0.1175,
      },
      {
        amount: 0.9,
        fee: 0.1115,
      },
      {
        amount: 0.95,
        fee: 0.1065,
      },
      {
        amount: 1.0,
        fee: 0.1025,
      },
    ],
  },
  {
    months: 7,
    contributions: [
      {
        amount: 0.5,
        fee: 0.255,
      },
      {
        amount: 0.55,
        fee: 0.2275,
      },
      {
        amount: 0.6,
        fee: 0.1975,
      },
      {
        amount: 0.65,
        fee: 0.185,
      },
      {
        amount: 0.7,
        fee: 0.17,
      },
      {
        amount: 0.75,
        fee: 0.1575,
      },
      {
        amount: 0.8,
        fee: 0.1515,
      },
      {
        amount: 0.85,
        fee: 0.145,
      },
      {
        amount: 0.9,
        fee: 0.1375,
      },
      {
        amount: 0.95,
        fee: 0.1265,
      },
      {
        amount: 1.0,
        fee: 0.1175,
      },
    ],
  },
  {
    months: 8,
    contributions: [
      {
        amount: 0.6,
        fee: 0.24,
      },
      {
        amount: 0.65,
        fee: 0.2275,
      },
      {
        amount: 0.7,
        fee: 0.1975,
      },
      {
        amount: 0.75,
        fee: 0.185,
      },
      {
        amount: 0.8,
        fee: 0.17,
      },
      {
        amount: 0.85,
        fee: 0.165,
      },
      {
        amount: 0.9,
        fee: 0.1575,
      },
      {
        amount: 0.95,
        fee: 0.1495,
      },
      {
        amount: 1.0,
        fee: 0.145,
      },
    ],
  },
  {
    months: 9,
    contributions: [
      {
        amount: 0.65,
        fee: 0.255,
      },
      {
        amount: 0.7,
        fee: 0.2275,
      },
      {
        amount: 0.75,
        fee: 0.2125,
      },
      {
        amount: 0.8,
        fee: 0.1975,
      },
      {
        amount: 0.85,
        fee: 0.185,
      },
      {
        amount: 0.9,
        fee: 0.17,
      },
      {
        amount: 0.95,
        fee: 0.1615,
      },
      {
        amount: 1.0,
        fee: 0.1575,
      },
    ],
  },
  {
    months: 10,
    contributions: [
      {
        amount: 0.7,
        fee: 0.255,
      },
      {
        amount: 0.75,
        fee: 0.24,
      },
      {
        amount: 0.8,
        fee: 0.2275,
      },
      {
        amount: 0.85,
        fee: 0.2125,
      },
      {
        amount: 0.9,
        fee: 0.1975,
      },
      {
        amount: 0.95,
        fee: 0.185,
      },
      {
        amount: 1.0,
        fee: 0.17,
      },
    ],
  },
  {
    months: 11,
    contributions: [
      {
        amount: 0.8,
        fee: 0.255,
      },
      {
        amount: 0.85,
        fee: 0.2275,
      },
      {
        amount: 0.9,
        fee: 0.2125,
      },
      {
        amount: 0.95,
        fee: 0.2025,
      },
      {
        amount: 1.0,
        fee: 0.1975,
      },
    ],
  },
  {
    months: 12,
    contributions: [
      {
        amount: 0.85,
        fee: 0.255,
      },
      {
        amount: 0.9,
        fee: 0.24,
      },
      {
        amount: 0.95,
        fee: 0.2275,
      },
      {
        amount: 1.0,
        fee: 0.2125,
      },
    ],
  },
];
