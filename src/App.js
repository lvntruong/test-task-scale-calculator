import { useEffect, useState } from "react";
import "./App.css";
import "./frontend-lite.min.css";
import "./custom.css";
import { nFormatter, advances } from "./helper";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import Slider from "@mui/material/Slider";
import Snackbar from "@mui/material/Snackbar";
import Alert from "@mui/material/Alert";

const SimpleMaskMoney = window.SimpleMaskMoney;

function App() {
  // const [stateEarnings, setStateEarnings] = useState("$2,500.00");
  const [isSnackBarOpen, setIsSnackBarOpen] = useState(false);

  useEffect(() => {
    calcInit();
  }, []);

  function calcInit() {
    var urlParams = new URLSearchParams(window.location.search);
    if (urlParams.has("earnings")) {
      var urlEarnings = urlParams.get("earnings");
    }
    if (urlParams.has("firstname")) {
      var urlFirstName = urlParams.get("firstname");
      document.getElementById("first_name").value = urlFirstName;
    }
    if (urlParams.has("lastname")) {
      var urlLastName = urlParams.get("lastname");
      document.getElementById("last_name").value = urlLastName;
    }
    if (urlParams.has("email")) {
      var urlEmail = urlParams.get("email");
      document.getElementById("email").value = urlEmail;
    }
    if (urlParams.has("stemclient")) {
      document.getElementById("stem-user").setAttribute("checked", true);
    }

    if (urlEarnings) {
      document.getElementById("earnings").value = SimpleMaskMoney.formatToMask(
        urlEarnings * 100,
        {
          allowNegative: false,
          negativeSignAfter: false,
          prefix: "$",
          suffix: "",
          fixed: true,
          fractionDigits: 2,
          decimalSeparator: ".",
          thousandsSeparator: ",",
          cursor: "move",
        }
      );
    } else {
      document.getElementById("earnings").value = SimpleMaskMoney.formatToMask(
        1000 * 100,
        {
          allowNegative: false,
          negativeSignAfter: false,
          prefix: "$",
          suffix: "",
          fixed: true,
          fractionDigits: 2,
          decimalSeparator: ".",
          thousandsSeparator: ",",
          cursor: "move",
        }
      );
    }

    document.getElementById("multiplier").value = 8;
    document.getElementById("contribution").value = 75;
    setMinContribution();
    outputAdvanceAmount();
    outputContribution();
    outputOffer();
    pullAdvanceIntoModal();
  }

  function outputAdvanceAmount() {
    var earnings = SimpleMaskMoney.formatToNumber(
      document.getElementById("earnings").value,
      { decimalSeparator: ".", thousandsSeparator: "," }
    );
    var multiplier = document.getElementById("multiplier").value;
    var advanceAmount = SimpleMaskMoney.formatToMask(
      (earnings * multiplier).toFixed(2),
      {
        prefix: "$",
        thousandsSeparator: ",",
        decimalSeparator: ".",
        fractionDigits: 2,
      }
    ).slice(0, -3);
    var minAdvanceAmount = nFormatter((earnings * 2).toFixed(2), 1);
    var maxAdvanceAmount = nFormatter((earnings * 12).toFixed(2), 1);
    var earningsLimit = 250000;
    var urlParams = new URLSearchParams(window.location.search);
    if (urlParams.has("nolimit")) {
      earningsLimit = 999999999;
      document.getElementById("earnings").setAttribute("maxlength", 16);
    }

    if (earnings < 500 || earnings > earningsLimit) {
      //console.log('earniings less than 100');
      document.getElementById("calculatorInteract").style.opacity = 0.25;
      document.getElementById("advanceOutputContainer").style.opacity = 0.25;
      document.getElementById("scale-apply-button").style.opacity = 0.25;
      document.getElementById("sliderFiller").style.opacity = 0.25;
      document.getElementById("multiplier").setAttribute("disabled", true);
      document.getElementById("contribution").setAttribute("disabled", true);
      document
        .getElementById("scale-apply-button")
        .setAttribute("disabled", true);
      document.getElementById("earnings").style =
        "border: 2px solid var(--scale-text-alt-2-color)";
      if (earnings < 500) {
        document.getElementById("calcWarningLow").style.opacity = 1;
        document.getElementById("calcWarningLow").style.display = "block";
        document.getElementById("calcWarningHigh").style.display = "none";
      }
      if (earnings > earningsLimit) {
        document.getElementById("calcWarningHigh").style.opacity = 1;
        document.getElementById("calcWarningHigh").style.display = "block";
        document.getElementById("calcWarningLow").style.display = "none";
      }
    } else {
      document.getElementById("earnings").style = "border: 0";
      document.getElementById("calcWarningLow").style.opacity = 0;
      document.getElementById("calcWarningLow").style.display = "block";
      document.getElementById("calcWarningHigh").style.display = "none";
      document.getElementById("calculatorInteract").style.opacity = 1;
      document.getElementById("advanceOutputContainer").style.opacity = 1;
      document.getElementById("scale-apply-button").style.opacity = 1;
      document.getElementById("sliderFiller").style.opacity = 1;
      document.getElementById("multiplier").removeAttribute("disabled");
      document.getElementById("contribution").removeAttribute("disabled");
      document.getElementById("scale-apply-button").removeAttribute("disabled");
      document.getElementById("advanceAmount").innerHTML = advanceAmount;
      document.getElementById("outputBoxAdvanceAmount").innerHTML =
        advanceAmount;
      document.getElementById("minAdvance").innerHTML = "$" + minAdvanceAmount;
      document.getElementById("maxAdvance").innerHTML = "$" + maxAdvanceAmount;
      setMinContribution();
    }
  }

  function outputDuration(fee) {
    var earnings = SimpleMaskMoney.formatToNumber(
      document.getElementById("earnings").value,
      { decimalSeparator: ".", thousandsSeparator: "," }
    );
    var multiplier = document.getElementById("multiplier").value;
    var advanceAmount = earnings * multiplier;
    var contribution = document.getElementById("contribution").value;
    var monthlyPayment = (earnings * (contribution / 100)).toFixed(2);
    var advancePayback = (advanceAmount + advanceAmount * (fee / 100)).toFixed(
      2
    );
    var advancePaybackFormatted = SimpleMaskMoney.formatToMask(advancePayback, {
      prefix: "$",
      thousandsSeparator: ",",
      decimalSeparator: ".",
      fractionDigits: 2,
    });
    var advanceDuration = Math.ceil(advancePayback / monthlyPayment);
    document.getElementById("advanceDuration").innerHTML =
      advanceDuration + "<sup> Months</sup>";
    document.getElementById("advancePayback").innerHTML =
      advancePaybackFormatted.slice(0, -3) +
      "<span class='decimals'>" +
      advancePaybackFormatted.substr(advancePaybackFormatted.length - 3) +
      "</span>";
    var advanceFeeAmount = (advanceAmount * (fee / 100)).toFixed(2);
    var advanceFeeAmountFormatted = SimpleMaskMoney.formatToMask(
      advanceFeeAmount,
      {
        prefix: "$",
        thousandsSeparator: ",",
        decimalSeparator: ".",
        fractionDigits: 2,
      }
    );
    document.getElementById("advanceFeeAmount").innerHTML =
      advanceFeeAmountFormatted.slice(0, -3) +
      "<span class='decimals'>" +
      advanceFeeAmountFormatted.substr(advanceFeeAmountFormatted.length - 3) +
      "</span>";
    document.getElementById("advancePayment").innerHTML = monthlyPayment;
    // Output all vars to the consoole for debugging
    //console.log('Earnings: ' + earnings + '\n' + 'Multiplier: ' + multiplier + '\n' + 'Advance Amount: ' + advanceAmount + '\n' + 'Contribution: ' + contribution + '\n' + 'Fee: ' + fee + '\n' + 'Monthly Payment: ' + monthlyPayment + '\n' + 'Advance Payback: ' + advancePayback + '\n' + 'Advance Duration: ' + advanceDuration);
  }

  function setMinContribution() {
    var advanceLookup = document.getElementById("multiplier").value - 2;
    //console.log(advanceLookup);
    var minContribution = Math.round(
      advances[advanceLookup].contributions[0].amount * 100
    );
    //console.log(minContribution);
    var sliderUnits = (100 - minContribution) / 5 + 1;
    var fillerUnits = 18 - sliderUnits;
    //console.log("Filler Units: " + fillerUnits + " Slider Units: " + sliderUnits);
    document.getElementById("contributionContainer").style.cssText =
      "grid-template-columns: " + fillerUnits + "fr " + sliderUnits + "fr;";
    document
      .getElementById("contribution")
      .setAttribute("min", minContribution);
    let contributionWidth = document.getElementById("contribution").offsetWidth;
    var contributionPadding = (
      ((contributionWidth / sliderUnits - 12) / 2 / contributionWidth) *
      100
    ).toFixed(2);
    document.getElementById("contribution").style.cssText +=
      "padding: 0px " + contributionPadding + "%;";
    if (fillerUnits == 0) {
      document.getElementById("sliderFiller").style.visibility = "hidden";
      document.getElementById("contribution").style.cssText +=
        "border-radius: 100px;";
    } else {
      document.getElementById("sliderFiller").style.visibility = "visible";
      document.getElementById("contribution").style.cssText +=
        "border-radius: 0px 100px 100px 0px;";
    }
    outputOffer();
  }

  function outputOffer() {
    outputContribution();
    var advanceLookup = document.getElementById("multiplier").value - 2;
    var minContribution = document
      .getElementById("contribution")
      .getAttribute("min");
    var contributionSteps = (100 - minContribution) / 5;
    //console.log(contributionSteps);
    var contributionLookup =
      (document.getElementById("contribution").value - 100) / 5 +
      contributionSteps;
    //console.log(contributionLookup);
    var advanceFee = (
      advances[advanceLookup].contributions[contributionLookup].fee * 100
    ).toFixed(2);
    document.getElementById("advanceFee").innerHTML = advanceFee + "%";
    outputDuration(advanceFee);
  }

  function outputContribution() {
    var contribution = document.getElementById("contribution").value;
    document.getElementById("advanceContribution").innerHTML =
      contribution + "%";
  }

  function closeModal() {
    var modal = document.getElementById("modal");
    var modalOverlay = document.getElementById("modal-overlay");
    modal.classList.add("closed");
    modalOverlay.classList.add("closed");
  }

  function openModal() {
    var modal = document.getElementById("modal");
    var modalOverlay = document.getElementById("modal-overlay");
    modal.classList.remove("closed");
    modalOverlay.classList.remove("closed");
    pullAdvanceIntoModal();
  }

  function pullAdvanceIntoModal() {
    var applyAdvanceAmount = document.getElementById(
      "outputBoxAdvanceAmount"
    ).innerHTML;
    var applyContribution = document.getElementById("contribution").value + "%";
    var applyDuration = document.getElementById("advanceDuration").innerHTML;
    var applyFee = document.getElementById("advanceFeeAmount").innerHTML;
    var applyFeePct = document.getElementById("advanceFee").innerHTML;
    var applyPayback = document.getElementById("advancePayback").innerHTML;
    document.getElementById("apply-advance-amount").innerHTML =
      applyAdvanceAmount;
    document.getElementById("apply-contribution").innerHTML = applyContribution;
    document.getElementById("apply-duration").innerHTML =
      parseInt(applyDuration) + " Months";
    document.getElementById("apply-fee").innerHTML = applyFee;
    document.getElementById("apply-fee-pct").innerHTML = applyFeePct;
    document.getElementById("apply-payback").innerHTML = applyPayback;

    // Populate the hidden fields for the form
    var earnings = document.getElementById("earnings").value;
    var termsSubmitted = document.querySelector(
      "div.scale-apply-details"
    ).textContent;
    document.getElementById("00N3m00000PvAj2").value = earnings.substring(1);
    document.getElementById("00N3m00000PvAid").value = applyAdvanceAmount;
    document.getElementById("00N3m00000PvAii").value = applyContribution;
    document.getElementById("00N3m00000PvAin").value = applyFeePct;
    document.getElementById("00N3m00000PvAis").value = termsSubmitted;

    toggleFields();
  }

  function toggleFields() {
    var stemUser = document.getElementById("stem-user").checked;
    var notStemUser = document.getElementById("not-stem-user").checked;
    var stemEmail = document.getElementById("stem-email-container");
    var whoAdvance = document.getElementById("who-advance-container");
    var orgType = document.getElementById("org-type-container");
    var hiddenField = document.getElementById("00N1N00000PuXYi");
    var hiddenOrgType = document.getElementById("00N1N00000F9l33");
    if (stemUser) {
      stemEmail.style.display = "block";
      whoAdvance.style.display = "none";
      orgType.style.display = "none";
      hiddenField.value = "Yes";
      hiddenOrgType.value = "null";
    } else {
      stemEmail.style.display = "none";
    }

    if (notStemUser) {
      whoAdvance.style.display = "block";
      orgType.style.display = "block";
      document.getElementById("00N3m00000PvAi9").setAttribute("required", true);
      document.getElementById("00N1N00000F9l33").setAttribute("required", true);
      hiddenField.value = "No";
    } else {
      whoAdvance.style.display = "none";
      orgType.style.display = "none";
      document.getElementById("00N3m00000PvAi9").removeAttribute("required");
      document.getElementById("00N1N00000F9l33").removeAttribute("required");
    }
  }

  function outputAdvanceAmount() {
    var earnings = SimpleMaskMoney.formatToNumber(
      document.getElementById("earnings").value,
      { decimalSeparator: ".", thousandsSeparator: "," }
    );
    var multiplier = document.getElementById("multiplier").value;
    var advanceAmount = SimpleMaskMoney.formatToMask(
      (earnings * multiplier).toFixed(2),
      {
        prefix: "$",
        thousandsSeparator: ",",
        decimalSeparator: ".",
        fractionDigits: 2,
      }
    ).slice(0, -3);
    var minAdvanceAmount = nFormatter((earnings * 2).toFixed(2), 1);
    var maxAdvanceAmount = nFormatter((earnings * 12).toFixed(2), 1);
    var earningsLimit = 250000;
    var urlParams = new URLSearchParams(window.location.search);
    if (urlParams.has("nolimit")) {
      earningsLimit = 999999999;
      document.getElementById("earnings").setAttribute("maxlength", 16);
    }

    if (earnings < 500 || earnings > earningsLimit) {
      //console.log('earniings less than 100');
      document.getElementById("calculatorInteract").style.opacity = 0.25;
      document.getElementById("advanceOutputContainer").style.opacity = 0.25;
      document.getElementById("scale-apply-button").style.opacity = 0.25;
      document.getElementById("sliderFiller").style.opacity = 0.25;
      document.getElementById("multiplier").setAttribute("disabled", true);
      document.getElementById("contribution").setAttribute("disabled", true);
      document
        .getElementById("scale-apply-button")
        .setAttribute("disabled", true);
      document.getElementById("earnings").style =
        "border: 2px solid var(--scale-text-alt-2-color)";
      if (earnings < 500) {
        document.getElementById("calcWarningLow").style.opacity = 1;
        document.getElementById("calcWarningLow").style.display = "block";
        document.getElementById("calcWarningHigh").style.display = "none";
      }
      if (earnings > earningsLimit) {
        document.getElementById("calcWarningHigh").style.opacity = 1;
        document.getElementById("calcWarningHigh").style.display = "block";
        document.getElementById("calcWarningLow").style.display = "none";
      }
    } else {
      document.getElementById("earnings").style = "border: 0";
      document.getElementById("calcWarningLow").style.opacity = 0;
      document.getElementById("calcWarningLow").style.display = "block";
      document.getElementById("calcWarningHigh").style.display = "none";
      document.getElementById("calculatorInteract").style.opacity = 1;
      document.getElementById("advanceOutputContainer").style.opacity = 1;
      document.getElementById("scale-apply-button").style.opacity = 1;
      document.getElementById("sliderFiller").style.opacity = 1;
      document.getElementById("multiplier").removeAttribute("disabled");
      document.getElementById("contribution").removeAttribute("disabled");
      document.getElementById("scale-apply-button").removeAttribute("disabled");
      document.getElementById("advanceAmount").innerHTML = advanceAmount;
      document.getElementById("outputBoxAdvanceAmount").innerHTML =
        advanceAmount;
      document.getElementById("minAdvance").innerHTML = "$" + minAdvanceAmount;
      document.getElementById("maxAdvance").innerHTML = "$" + maxAdvanceAmount;
      setMinContribution();
    }
  }

  const onSubmitForm = (e) => {
    e.preventDefault();
    console.log(e);
    setIsSnackBarOpen(true);
    closeModal();
  };

  return (
    <div className="App elementor">
      <Snackbar
        anchorOrigin={{ vertical: "top", horizontal: "center" }}
        open={isSnackBarOpen}
        autoHideDuration={6000}
        onClose={() => setIsSnackBarOpen(false)}
      >
        {/* <Alert onClose={() => {}} severity="success" sx={{ width: "100%" }}>
          This is a success message!
        </Alert> */}
        <Alert severity="error">This is an error message!</Alert>
      </Snackbar>
      <section
        className="elementor-section elementor-top-section elementor-element elementor-element-f6f0c6d elementor-section-boxed elementor-section-height-default elementor-section-height-default"
        data-id="f6f0c6d"
        data-element_type="section"
        data-settings='{"background_background":"classic"}'
      >
        <div className="elementor-container elementor-column-gap-default">
          <div
            className="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-dd774ef"
            data-id="dd774ef"
            data-element_type="column"
          >
            <div className="elementor-widget-wrap elementor-element-populated">
              <div
                className="elementor-element elementor-element-f013eca elementor-widget elementor-widget-menu-anchor"
                data-id="f013eca"
                data-element_type="widget"
                data-widget_type="menu-anchor.default"
              >
                <div className="elementor-widget-container">
                  <div
                    id="scale-calculator"
                    className="elementor-menu-anchor"
                  />
                </div>
              </div>
              <div
                className="elementor-element elementor-element-fc37d9d elementor-widget elementor-widget-heading"
                data-id="fc37d9d"
                data-element_type="widget"
                data-widget_type="heading.default"
              >
                <div className="elementor-widget-container">
                  <h2 className="elementor-heading-title elementor-size-default">
                    Scale Calculator—
                  </h2>
                </div>
              </div>
              <div
                className="elementor-element elementor-element-ba17650 elementor-widget elementor-widget-heading"
                data-id="ba17650"
                data-element_type="widget"
                data-widget_type="heading.default"
              >
                <div className="elementor-widget-container">
                  <h5 className="elementor-heading-title elementor-size-default">
                    Outline the terms of your advance and estimate payback.*
                  </h5>
                </div>
              </div>
              <div
                className="elementor-element elementor-element-7052a8f elementor-widget elementor-widget-ekit_wb_63636"
                data-id="7052a8f"
                data-element_type="widget"
                data-widget_type="ekit_wb_63636.default"
              >
                <div className="elementor-widget-container">
                  <div className="calc-wrap">
                    <section id="scale-calculator">
                      <div className="dontmatter">
                        <h2 className="scale-centered"></h2>
                        <p className="scale-centered scale-v-spacer-l">
                          <span className="scale-medium"> </span>
                        </p>
                        <label htmlFor="earnings">
                          My monthly income from digital sound recordings is:
                          {/* <input
                            className="form-control scale-earnings scale-medium scale-v-spacer-m"
                            inputMode="numeric"
                            id="earnings"
                            name="earnings"
                            placeholder="$2,500.00"
                            maxLength={15}
                            // value={stateEarnings}
                            onChange={(e) => {
                              document.getElementById("earnings").value =
                                SimpleMaskMoney.formatToMask(e.target.value, {
                                  allowNegative: false,
                                  negativeSignAfter: false,
                                  prefix: "$",
                                  suffix: "",
                                  fixed: true,
                                  fractionDigits: 2,
                                  decimalSeparator: ".",
                                  thousandsSeparator: ",",
                                  cursor: "move",
                                });
                              outputAdvanceAmount();
                            }}
                            style={{ border: "0px" }}
                          /> */}
                          <TextField
                            className="form-control scale-earnings scale-medium scale-v-spacer-m"
                            defaultValue="$2,500.00"
                            id="earnings"
                            label="Earnings"
                            variant="outlined"
                            maxLength={15}
                            onChange={(e) => {
                              document.getElementById("earnings").value =
                                SimpleMaskMoney.formatToMask(e.target.value, {
                                  allowNegative: false,
                                  negativeSignAfter: false,
                                  prefix: "$",
                                  suffix: "",
                                  fixed: true,
                                  fractionDigits: 2,
                                  decimalSeparator: ".",
                                  thousandsSeparator: ",",
                                  cursor: "move",
                                });
                              outputAdvanceAmount();
                            }}
                          />
                        </label>
                        <p
                          id="calcWarningLow"
                          className="scale-centered calcWarning"
                          style={{ opacity: 0, display: "block" }}
                        ></p>
                        <p
                          id="calcWarningHigh"
                          className="scale-centered calcWarning"
                          style={{ display: "none", opacity: 1 }}
                        ></p>
                        <div
                          id="calculatorInteract"
                          className="scale-v-spacer-s"
                          style={{ opacity: 1 }}
                        >
                          <div className="slider-wrap">
                            <label htmlFor="multiplier">
                              Advance Amount
                              <br
                                style={{ display: "none" }}
                                className="scale-only-mobile"
                              />
                              <span className="scale-hide-mobile">:</span>
                              <output id="advanceAmount" name="advanceAmount">
                                $10,010
                              </output>
                            </label>
                            <div
                              className="sliderContainer"
                              id="multiplierSlider"
                            >
                              <p
                                className="sliderLabel minimum"
                                id="minAdvance"
                              >
                                $4k
                              </p>
                              <input
                                type="range"
                                id="multiplier"
                                name="multiplier"
                                min={2}
                                max={12}
                                step={1}
                                onChange={() => {
                                  outputAdvanceAmount();
                                }}
                                // oninput="outputAdvanceAmount();"
                                style={{
                                  display: "none",
                                }}
                              />
                              <Slider
                                aria-label="multiplier"
                                id="multiplier2"
                                name="multiplier2"
                                min={2}
                                max={12}
                                step={1}
                                defaultValue={8}
                                onChange={(e) => {
                                  document.getElementById("multiplier").value =
                                    e.target.value;
                                  outputAdvanceAmount();
                                }}
                                // style={{
                                //   padding: "0px 1.53%",
                                //   borderRadius: "0px 100px 100px 0px",
                                // }}
                              />
                              <p
                                className="sliderLabel maximum"
                                id="maxAdvance"
                              >
                                $24k
                              </p>
                            </div>
                          </div>
                          <div className="slider-wrap">
                            <label
                              htmlFor="contribution"
                              className="scale-v-spacer-m"
                            >
                              Payback Contribution
                              <br
                                style={{ display: "none" }}
                                className="scale-only-mobile"
                              />
                              <span className="scale-hide-mobile">:</span>
                              <output
                                id="advanceContribution"
                                name="advanceContribution"
                              >
                                85%
                              </output>
                            </label>
                            <div
                              className="sliderContainer"
                              id="contributionSlider"
                            >
                              <p className="sliderLabel minimum">15%</p>
                              <div
                                id="contributionContainer"
                                style={{ gridTemplateColumns: "4fr 14fr" }}
                              >
                                <div
                                  id="sliderFiller"
                                  style={{ visibility: "visible", opacity: 1 }}
                                />
                                <input
                                  type="range"
                                  id="contribution"
                                  name="contribution"
                                  max={100}
                                  step={5}
                                  onChange={() => {
                                    outputOffer();
                                  }}
                                  // oninput="outputOffer();"
                                  min={35}
                                  style={{
                                    display: "none",
                                    padding: "0px 1.53%",
                                    borderRadius: "0px 100px 100px 0px",
                                  }}
                                />
                                <Slider
                                  aria-label="contribution"
                                  id="contribution2"
                                  name="contribution2"
                                  defaultValue={75}
                                  onChange={(e) => {
                                    console.log(e.target.value);
                                    document.getElementById(
                                      "contribution"
                                    ).value = e.target.value;
                                    outputOffer();
                                  }}
                                  step={5}
                                  min={35}
                                  max={100}
                                  // style={{
                                  //   padding: "0px 1.53%",
                                  //   borderRadius: "0px 100px 100px 0px",
                                  // }}
                                />
                              </div>
                              <p className="sliderLabel maximum">100%</p>
                            </div>
                            <p className="scale-centered">
                              Choose the % of your monthly earnings that you can
                              commit towards paying off the advance.
                            </p>
                          </div>
                        </div>
                        <div
                          id="advanceOutputContainer"
                          className="scale-v-spacer-l"
                          style={{ opacity: 1 }}
                        />
                        <label
                          htmlFor="advancePayment"
                          style={{ display: "none" }}
                        >
                          Monthly Payment: $
                          <mark>
                            <output id="advancePayment" name="advancePayment">
                              1701.70
                            </output>
                          </mark>
                        </label>
                      </div>
                    </section>
                    <section className="pie-chart">
                      <div className="pie">
                        <div className="months">
                          <output
                            id="advanceDuration"
                            className="pricenum"
                            name="advanceDuration"
                          >
                            7<sup> Months</sup>
                          </output>
                        </div>
                        <div className="advancefee">
                          <p>Total Advance Fee</p>
                          <output
                            id="advanceFeeAmount"
                            className="pricenum"
                            name="advanceFeeAmount"
                          >
                            $995<span className="decimals">.99</span>
                          </output>
                          <span id="advanceFee">9.95%</span> of
                          <span id="outputBoxAdvanceAmount">$10,010</span>. This
                          is our fixed, one-time fee.
                          <p />
                        </div>
                        <div className="pie-txt">
                          <p> Total Payback Amount</p>
                          <output
                            id="advancePayback"
                            className="pricenum"
                            name="advancePayback"
                          >
                            $11,005<span className="decimals">.99</span>
                          </output>
                        </div>
                        <svg
                          className="advance"
                          fwidth={300}
                          height={300}
                          viewBox="0 0 300 300"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            d="M-1.31134e-05 150C-2.03558e-05 67.1573 67.1573 2.03558e-05 150 1.31134e-05C232.843 5.87108e-06 300 67.1573 300 150C300 232.843 232.843 300 150 300C67.1573 300 -5.87108e-06 232.843 -1.31134e-05 150ZM240 150C240 100.294 199.706 60 150 60C100.294 60 60 100.294 60 150C60 199.706 100.294 240 150 240C199.706 240 240 199.706 240 150Z"
                            fill="#A0DFD9"
                          />
                        </svg>
                        <svg
                          className="total"
                          width={300}
                          height={300}
                          viewBox="0 0 300 300"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            d="M150 300C117.8 300 86.4539 289.638 60.5978 270.446C34.7417 251.254 15.7479 224.251 6.4248 193.43C-2.8983 162.609 -2.05571 129.606 8.82802 99.3002C19.7117 68.9949 40.0587 42.9964 66.8604 25.1488C93.6622 7.30132 125.496 -1.44771 157.654 0.195447C189.813 1.83857 220.589 13.7867 245.432 34.273C270.275 54.7593 287.866 82.6963 295.604 113.953C303.342 145.21 300.817 178.127 288.401 207.838L233.041 184.703C240.49 166.876 242.005 147.126 237.363 128.372C232.72 109.618 222.165 92.8556 207.259 80.5638C192.353 68.272 173.888 61.1031 154.593 60.1173C135.297 59.1314 116.197 64.3808 100.116 75.0893C84.0352 85.7978 71.827 101.397 65.2968 119.58C58.7666 137.763 58.261 157.565 63.8549 176.058C69.4487 194.551 80.845 210.752 96.3587 222.268C111.872 233.783 130.68 240 150 240L150 300Z"
                            fill="#89C7A2"
                          />
                        </svg>
                      </div>
                      <div className="elementor-widget-container">
                        <div className="elementor-button-wrapper">
                          <a
                            id="scale-apply-button"
                            // className="elementor-button-link elementor-button elementor-size-sm"
                            onClick={() => openModal()}
                            role="button"
                            style={{ opacity: 1 }}
                          >
                            <span className="elementor-button-content-wrapper">
                              {/* <span className="elementor-button-text">
                                Apply for Scale
                              </span> */}
                              <Button
                                className="elementor-button-text"
                                style={{
                                  backgroundColor: "#000000",
                                  padding: 20,
                                }}
                                variant="contained"
                              >
                                Apply for Scale
                              </Button>
                            </span>
                          </a>
                        </div>
                      </div>
                    </section>
                    <div
                      className="modal-overlay closed"
                      id="modal-overlay"
                      onclick="closeModal();"
                    />
                    <div className="modal closed" id="modal">
                      <button
                        className="close-button"
                        id="close-button"
                        onClick={() => closeModal()}
                      >
                        ×
                      </button>
                      <div className="modal-guts">
                        <h2 className="scale-v-spacer-m">Scale Application</h2>
                        <p className="scale-v-spacer-m">
                          Nice, you are one step closer to cash. Tell us more
                          about you so we may review your advance. After you
                          submit, we’ll verify your earnings and reach out with
                          next steps!
                        </p>
                        <form
                          className="scale-v-spacer-m"
                          // action="https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8"
                          method="POST"
                          onSubmit={onSubmitForm}
                        >
                          <div className="apply-now__list">
                            <TextField
                              className="form-control"
                              id="first_name"
                              label="First Name"
                              name="first_name"
                              variant="outlined"
                              type="text"
                              maxLength={40}
                              required
                              style={{
                                width: "100%",
                                marginBottom: 10,
                              }}
                            />
                            {/* <label
                                htmlFor="first_name"
                                className="scale-subtle-label"
                              >
                                First Name <span className="required">*</span>
                              </label>
                              <input
                                className="form-control"
                                id="first_name"
                                maxLength={40}
                                name="first_name"
                                type="text"
                                required
                              /> */}
                            {/* <li className="form-group form-group--right">
                              <label
                                htmlFor="last_name"
                                className="scale-subtle-label"
                              >
                                Last Name <span className="required">*</span>
                              </label>
                              <input
                                className="form-control"
                                id="last_name"
                                maxLength={80}
                                name="last_name"
                                type="text"
                                required
                              />
                            </li> */}
                            <TextField
                              className="form-control"
                              id="last_name"
                              label="Last Name"
                              name="last_name"
                              variant="outlined"
                              type="text"
                              maxLength={80}
                              required
                              style={{
                                width: "100%",
                                marginBottom: 10,
                              }}
                            />
                            {/* <li className="form-group">
                              <label
                                htmlFor="email"
                                className="scale-subtle-label"
                              >
                                Email <span className="required">*</span>
                              </label>
                              <input
                                className="form-control"
                                id="email"
                                maxLength={80}
                                name="email"
                                type="email"
                                required
                              />
                            </li> */}
                            <TextField
                              className="form-control"
                              id="email"
                              label="Email"
                              name="email"
                              variant="outlined"
                              type="email"
                              maxLength={80}
                              required
                              style={{
                                width: "100%",
                                marginBottom: 10,
                              }}
                            />
                            <li className="form-group">
                              <label htmlFor="stem-client">
                                Are you a Stem client?
                                <span className="required">*</span>
                              </label>
                              <input
                                className="scale-radio"
                                id="stem-user"
                                defaultValue="Yes"
                                name="stem-client"
                                type="radio"
                                required
                                onChange={() => {
                                  toggleFields();
                                }}
                              />
                              <label
                                className="scale-radio"
                                htmlFor="stem-user"
                              >
                                Yes
                              </label>
                              <input
                                className="scale-radio"
                                id="not-stem-user"
                                defaultValue="No"
                                name="stem-client"
                                type="radio"
                                required
                                onChange={() => {
                                  toggleFields();
                                }}
                              />
                              <label
                                className="scale-radio"
                                htmlFor="not-stem-user"
                              >
                                No
                              </label>
                            </li>
                            <li
                              className="form-group"
                              id="stem-email-container"
                              style={{ display: "none" }}
                            >
                              <label
                                htmlFor="00N3m00000PvAi4"
                                className="scale-subtle-label"
                              >
                                Stem Account Email (if different from above)
                              </label>
                              <input
                                className="form-control"
                                id="00N3m00000PvAi4"
                                maxLength={80}
                                name="00N3m00000PvAi4"
                                type="email"
                              />
                            </li>
                            <li
                              className="form-group"
                              id="org-type-container"
                              style={{ display: "none" }}
                            >
                              <label
                                htmlFor="00N1N00000F9l33"
                                className="scale-subtle-label"
                              >
                                Entity Requesting the Advance
                                <span className="required">*</span>
                              </label>
                              <select
                                className="form-control"
                                id="00N1N00000F9l33"
                                name="00N1N00000F9l33"
                                title="Lead Type"
                                value
                              >
                                <option value="Artist">Artist</option>
                                <option value="Management">Management</option>
                                <option value="Label">Label</option>
                                <option value="Other">Other</option>
                              </select>
                            </li>
                            <li
                              className="form-group"
                              id="who-advance-container"
                              style={{ display: "none" }}
                            >
                              <label
                                htmlFor="00N3m00000PvAi9"
                                className="scale-subtle-label"
                              >
                                Entity Name <span className="required">*</span>
                              </label>
                              <input
                                className="form-control"
                                id="00N3m00000PvAi9"
                                maxLength={80}
                                name="00N3m00000PvAi9"
                                type="text"
                              />
                            </li>
                            {/* <li
                              className="form-group"
                              id="who-advance-container"
                            >
                              <label
                                htmlFor="00N1N00000PLUFJ"
                                className="scale-subtle-label"
                              >
                                Spotify Profile URL
                              </label>
                              <input
                                className="form-control"
                                id="00N1N00000PLUFJ"
                                name="00N1N00000PLUFJ"
                                type="text"
                              />
                            </li> */}
                            <TextField
                              className="form-control"
                              id="00N1N00000PLUFJ"
                              label="Spotify Profile URL"
                              name="00N1N00000PLUFJ"
                              variant="outlined"
                              type="text"
                              required
                              style={{
                                width: "100%",
                                marginBottom: 10,
                              }}
                            />
                            {/* <li
                              className="form-group"
                              id="who-advance-container"
                            >
                              <label
                                htmlFor="url"
                                className="scale-subtle-label"
                              >
                                Website
                              </label>
                              <input
                                className="form-control"
                                id="url"
                                name="url"
                                type="text"
                              />
                            </li> */}
                            <TextField
                              className="form-control"
                              id="url"
                              label="Website"
                              name="url"
                              variant="outlined"
                              type="text"
                              maxLength={80}
                              required
                              style={{
                                width: "100%",
                                marginBottom: 10,
                              }}
                            />
                          </div>
                          <h3 className="scale-centered scale-v-spacer-l">
                            Your Advance Summary
                          </h3>
                          <div className="scale-apply-details scale-v-spacer-m">
                            <p>Advance Amount:</p>
                            <p>
                              <span id="apply-advance-amount">$8,000</span>
                            </p>
                            <p>Payback Contribution:</p>
                            <p>
                              <span id="apply-contribution">75%</span>
                            </p>
                            <p>Payback Duration:</p>
                            <p>
                              <span id="apply-duration">13 Months</span>
                            </p>
                            <p>Advance Fee:</p>
                            <p>
                              <span id="apply-fee">
                                $1,480<span className="decimals">.00</span>
                              </span>
                              / <span id="apply-fee-pct">18.50%</span>
                            </p>
                            <p>Total Payback Amount:</p>
                            <p>
                              <span id="apply-payback">
                                $9,480<span className="decimals">.00</span>
                              </span>
                            </p>
                          </div>
                          <div className="scale-apply-button-container scale-v-spacer-l">
                            <button
                              type="submit"
                              className="button button--primary button--rounded scale-button scale-button-hero"
                            >
                              Submit for Review
                            </button>
                          </div>
                          <input
                            type="hidden"
                            name="oid"
                            defaultValue="00D1N000002qRXu"
                          />
                          <input
                            type="hidden"
                            name="retURL"
                            defaultValue="https://stem.is/scale-thank-you"
                          />
                          <input
                            style={{ display: "none !important" }}
                            type="hidden"
                            name="recordType"
                            id="recordType"
                            defaultValue="0123m000000r6Ik"
                          />
                          <input
                            style={{ display: "none !important" }}
                            type="hidden"
                            name="00N3m00000PvAj2"
                            id="00N3m00000PvAj2"
                            defaultValue="1,000.00"
                          />
                          <input
                            style={{ display: "none !important" }}
                            type="hidden"
                            name="00N3m00000PvAid"
                            id="00N3m00000PvAid"
                            defaultValue="$8,000"
                          />
                          <input
                            style={{ display: "none !important" }}
                            type="hidden"
                            name="00N3m00000PvAii"
                            id="00N3m00000PvAii"
                            defaultValue="75%"
                          />
                          <input
                            style={{ display: "none !important" }}
                            type="hidden"
                            name="00N3m00000PvAin"
                            id="00N3m00000PvAin"
                            defaultValue="18.50%"
                          />
                          <input
                            style={{ display: "none !important" }}
                            type="hidden"
                            name="00N3m00000PvAis"
                            id="00N3m00000PvAis"
                            defaultValue="
Advance Amount:$8,000
Payback Contribution:75%
Payback Duration:13 Months
Advance Fee:$1,480.00 / 18.50%
Total Payback Amount:$9,480.00
"
                          />
                          <input
                            style={{ display: "none !important" }}
                            type="hidden"
                            name="00N1N00000PuXYi"
                            id="00N1N00000PuXYi"
                          />
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}

export default App;
